library(nnet)

seeds <- read.csv('seeds.csv', header = T)
seedstrain <- sample(1:210, 147)
seedstest <- setdiff(1:210, seedstrain)

ideal <- class.ind(seeds$class)
net = nnet(seeds[seedstrain, -8], ideal[seedstrain, ], size = 10,
                softmax = TRUE)

predicted <- predict(net, seeds[seedstest, -8], type = 'class')
# predict(net, c(15.26, 14.84, 0.8710, 5.763, 3.312, 2.2210, 5.220), type = 'class')
table(predicted, seeds[seedstest,]$class)
